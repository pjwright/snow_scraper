#!/usr/bin/env python

# Script to plot JH snow data
# Patrick Wright
# University of Montana
# Dec. 2014

# Usage:
# python snow_scraper_v2.py <argv1> <argv2> <argv3> 
# <argv1> is snow and air temp data, can be "raymer", "mid", or "rendezvous"
# <argv2> is wind data, can be "raymer" or "summit"
# <argv3> is time resolution, can be "1" for 1 hour, or "15" for 15 min

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import sys

print "Sprayfoam calculator"

snow_site = sys.argv[1]
wind_site = sys.argv[2]
time_res = sys.argv[3]

#-----------------------------------------------------------------------------------------
# SETUP PARAMETERS FOR FILE IMPORT
#-----------------------------------------------------------------------------------------
# If a plot is broken, use data print statements and tweak parameters below until
# the program is happy. Likely need to change the 'nrows' parameter.

# SNOW SITES, 1 HR
if snow_site == 'raymer' and time_res == '1':
  url_snow = 'http://wxstns.net/wxstns/jhnet/RAYMER.txt'
  title = "Raymer Study Plot 9,360' (hourly)"
  name_list=['Date','Time','NaN1','Battery','NaN2','NaN3','Air_Temp','NaN4','NaN5',
  'RH','NaN6','NaN7','Int_snow','NaN8','NaN9','Ttl_Snow','NaN9','Precip']
  nrows_snow = 154
  skiprows_snow = 6
if snow_site == 'mid' and time_res == '1':
  url_snow = 'http://wxstns.net/wxstns/jhnet/MID.txt'
  title = "Mid Mountain Study Plot 8,180' (hourly)"
  name_list=['Date','Time','NaN1','Battery','NaN2','NaN3','Air_Temp','NaN4','NaN5',
  'RH','NaN6','NaN7','Int_snow','NaN8','NaN9','Ttl_Snow','NaN9','Precip']
  nrows_snow = 154
  skiprows_snow = 6
if snow_site == 'rendezvous' and time_res == '1':
  url_snow = 'http://wxstns.net/wxstns/jhnet/RVBOWL.txt'
  title = "Rendezvous Bowl Plot 9,580' (hourly)"
  name_list=['Date','Time','NaN1','Battery','NaN2','NaN3','Air_Temp','NaN4','NaN5',
  'Int_snow','NaN8','NaN9','Ttl_Snow','NaN9','Precip']
  nrows_snow = 154
  skiprows_snow = 7
  
# SNOW SITES, 15 MIN
if snow_site == 'raymer' and time_res == '15':
  url_snow = 'http://wxstns.net/wxstns/jhnet/RAYMER2.txt'
  title = "Raymer Study Plot 9,360' (15 min)"
  name_list=['Date','Time','NaN1','Battery','NaN2','NaN3','Air_Temp','NaN4','NaN5',
  'RH','NaN6','NaN7','Int_snow','NaN8','NaN9','Ttl_Snow','NaN9','Precip']
  nrows_snow = 153
  skiprows_snow = 6
if snow_site == 'mid' and time_res == '15':
  url_snow = 'http://wxstns.net/wxstns/jhnet/MID2.txt'
  title = "Mid Mountain Study Plot 8,180' (15 min)"
  name_list=['Date','Time','NaN1','Battery','NaN2','NaN3','Air_Temp','NaN4','NaN5',
  'RH','NaN6','NaN7','Int_snow','NaN8','NaN9','Ttl_Snow','NaN9','Precip']
  nrows_snow = 152
  skiprows_snow = 6
if snow_site == 'rendezvous' and time_res == '15':
  url_snow = 'http://wxstns.net/wxstns/jhnet/RVBOWL2.txt'
  title = "Rendezvous Bowl Plot 9,580' (15 min)"
  name_list=['Date','Time','NaN1','Battery','NaN2','NaN3','Air_Temp','NaN4','NaN5',
  'Int_snow','NaN8','NaN9','Ttl_Snow','NaN9','Precip']
  nrows_snow = 154
  skiprows_snow = 7
  
# WIND SITES, 1 HR
if wind_site == 'raymer' and time_res == '1':
  url_wind = 'http://wxstns.net/wxstns/jhnet/RAYMRWND.txt'
  name_list_wind=['Date','Time','NaN1','Battery','NaN2','NaN3','Wind_Speed',
  'NaN4','Wind_Dir', 'NaN5','NaN6','Max_Gust']
  wind_title = "Wind at Raymer, 9,360'"
  nrows_wind = 154
  skiprows_wind = 6
if wind_site == 'summit' and time_res == '1':
  url_wind = 'http://wxstns.net/wxstns/jhnet/SUMMIT.txt'
  name_list_wind=['Date','Time','NaN1','NaN2','Summit_Air_Temp','NaN3','NaN4','Wind_Speed',
  'NaN5', 'Wind_Dir', 'NaN6', 'NaN7', 'Max_Gust'] 
  wind_title = "Wind at Summit, 10,450'"
  nrows_wind = 154
  skiprows_wind = 7
  
# WIND SITES, 15 MIN 
if wind_site == 'raymer' and time_res == '15':
  url_wind = 'http://wxstns.net/wxstns/jhnet/RAYMRWND2.txt'
  wind_title = "Wind at Raymer, 9,360'"
  name_list_wind=['Date','Time','NaN1','Battery','NaN2','NaN3','Wind_Speed',
  'NaN4','Wind_Dir', 'NaN5','NaN6','Max_Gust']
  nrows_wind = 153
  skiprows_wind = 6
if wind_site == 'summit' and time_res == '15':
  url_wind = 'http://wxstns.net/wxstns/jhnet/SUMMIT2.txt'
  wind_title = "Wind at Summit, 10,450'"
  name_list_wind=['Date','Time','NaN1','NaN2','Summit_Air_Temp','NaN3','NaN4','Wind_Speed',
  'NaN5', 'Wind_Dir', 'NaN6', 'NaN7', 'Max_Gust'] 
  nrows_wind = 154
  skiprows_wind = 7

#-----------------------------------------------------------------------------------------

def main():
  
  #----------------------------------------------------------------------------
  # FORMAT FOR SNOW DATA - hourly
  #----------------------------------------------------------------------------

  # Read in data:
  data = pd.io.parsers.read_csv(url_snow, skiprows=skiprows_snow, sep='  ', engine='python', 
  nrows=nrows_snow, index_col=False, header=None, names=name_list)
  #print data
  
  # Get a datestring that the 'infer_datetime_format' function can recognize
  datestring = data['Date'].apply(str) + data['Time'].apply(str)

  # Convert string to a Pandas datetime object
  dt = pd.to_datetime(datestring, infer_datetime_format=True)

  # set index of dataframe to datetime
  data_date = data.set_index(dt)
  data_date.sort_index(inplace=True)
  #print data_date

  #----------------------------------------------------------------------------
  # FORMAT FOR WIND DATA - hourly
  #----------------------------------------------------------------------------

  # Read in data:
  data_wind = pd.io.parsers.read_csv(url_wind, skiprows=skiprows_wind, sep='  ', engine='python', 
  nrows=nrows_wind, index_col=False, header=None, names=name_list_wind)
  #print data_wind

  #Get a datestring that the 'infer_datetime_format' function can recognize
  datestring = data_wind['Date'].apply(str) + data_wind['Time'].apply(str)

  #Convert string to a Pandas datetime object
  dt_wind = pd.to_datetime(datestring, infer_datetime_format=True)

  #set index of dataframe to datetime
  data_wind_date = data_wind.set_index(dt_wind)
  data_wind_date.sort_index(inplace=True)
  #print data_wind_date

  #----------------------------------------------------------------------------
  # PLOTTING...
  #----------------------------------------------------------------------------

  # plot window set to 17" x 8"
  fig = plt.figure(figsize=(17,8))

  # generate series for plotting x-axis for snow data
  x = data_date.index.to_pydatetime()    

  # AIR TEMP
  ax1 = fig.add_subplot(211)
  ax1.plot(x,data_date.Air_Temp, color='red')
  ax1.set_ylabel('Air Temp (F)',color='r')
  ax1.set_ylim([-20,40])
  ax1.set_title(title, fontsize=14)
  ax1.axhline(y=32.0, linewidth=0.5, color='r', zorder=0, ls='--')
  ax1.xaxis.set_ticklabels([])
  ax1.xaxis.grid(True, which='major')
  for tl in ax1.get_yticklabels():
      tl.set_color('r')
      
  # SNOW DEPTH
  ax12 = ax1.twinx()
  ax12.plot(x,data_date.Ttl_Snow, color='black')
  ax12.set_ylabel('Snow Depth (in)', color='black')
  ax12.set_ylim([(min(data_date.Ttl_Snow)-5),(max(data_date.Ttl_Snow) + 10)])
  for tl in ax12.get_yticklabels():
      tl.set_color('black')
      
  # NEW SNOW
  ax13 = ax1.twinx()
  ax13.spines['right'].set_position(('axes',1.05))
  ax13.set_frame_on(True)
  ax13.patch.set_visible(False)
  ax13.plot(x,data_date.Int_snow, color='b', label='new snow')
  ax13.set_ylabel('New Snow (in)', color='b')
  plt.ylim([0,15])
  for tl in ax13.get_yticklabels():
      tl.set_color('b')

  # WATER CONTENT
  ax14 = ax1.twinx()
  ax14.spines['right'].set_position(('axes',1.1))
  ax14.set_frame_on(True)
  ax14.patch.set_visible(False)
  ax14.plot(x,data_date.Precip, color='b', ls='--', label='water content')
  ax14.set_ylabel('Water Content (in)', color='b')
  plt.ylim([0,0.75])
  for tl in ax14.get_yticklabels():
      tl.set_color('b')
  h1, l1 = ax13.get_legend_handles_labels()
  h2, l2 = ax14.get_legend_handles_labels()
  ax14.legend(h1+h2, l1+l2, loc=3,prop={'size':10})

  # generate series for plotting x-axis for wind data
  x_wind = data_wind_date.index.to_pydatetime()

  # WIND DIRECTION
  ax2 = fig.add_subplot(212)
  ax2.plot(x_wind,data_wind_date.Wind_Dir, color='black')
  ax2.set_ylabel('Wind Direction', color='black')
  ax2.set_ylim([0,360])
  ax2.axhline(y=270.0, linewidth=0.5, color='black', zorder=0, ls='--')
  ax2.axhline(y=180.0, linewidth=0.5, color='black', zorder=0, ls='--')
  ax2.xaxis.grid(True, which='major')
  for tl in ax2.get_yticklabels():
      tl.set_color('black')

  # MAX GUST    
  ax22 = ax2.twinx()
  ax22.plot(x_wind,data_wind_date.Max_Gust, color='blue', ls='--', label='max gust')
  #ax22.set_ylabel('Max Gust (mph)', color='blue')
  ax22.set_ylim([0,60])
  ax22.axes.get_yaxis().set_visible(False)
  #ax22.spines['right'].set_position(('axes',1.07))
  #for tl in ax22.get_yticklabels():
      #tl.set_color('blue')

  # WIND SPEED
  ax23 = ax2.twinx()
  ax23.plot(x_wind, data_wind_date.Wind_Speed, label='wind speed')
  ax23.set_ylabel('Wind Speed (mph)', color='b')
  ax23.set_ylim([0,60])
  ax23.set_title(wind_title, fontsize=12)
  for tl in ax23.get_yticklabels():
      tl.set_color('b')
  h1, l1 = ax22.get_legend_handles_labels()
  h2, l2 = ax23.get_legend_handles_labels()
  ax23.legend(h1+h2, l1+l2, loc=3,prop={'size':10})

  #fig.subplots_adjust(bottom=0.3)
  fig.subplots_adjust(right=.87)
  fig.subplots_adjust(left=.06)        
  #fig.subplots_adjust(top=0.8)

  #from IPython import embed
  #embed()
  #plt.ion()
  #plt.draw() # execute this if updates are not being drawn on plot

  plt.show()

if __name__ == '__main__': 

  main()
